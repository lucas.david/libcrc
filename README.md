![libcrc](https://assets.gitlab-static.net/uploads/-/system/project/avatar/23807161/libcrc-blue.png?width=48) - an open-source CRC calculation header-only library providing a support for multiple platforms and architectures

## Getting Started

## Usage

### Design goals

- **Intuitive syntax.** Most of CRC libraries are currently C implementations, which, in comparison to C⁺⁺ are more verbose, or at least, less straightforward. This library is using operators and idioms of modern C⁺⁺ to simplify CRC calculation, either as an intuitive function `calculate` or as a functional structure with `operator()` overload. This library is intended to fit perfectly within standard library use. Also static meta-information on CRCs implementation are available.
- **Trivial integration.** Just include `<crc>`. No external library, no dependencies. `libcrc` is header only.
- **Speed and conciseness.** By using modern C⁺⁺, the goal is to reduce a maximum the footprint of `libcrc` without hurting performance. It is achieved essentially by heavily using _compile-time_ expression.
- **Testing.** As soon as the objective of `libcrc` is to be used on large variety of platforms, it is important to seriously test building and implementation of this library. For now, pipeline is set to check build and unit tests but further upgrades are on the line (e.g. coverage).
- **Accessibility.** For now `libcrc` only requires `C⁺⁺11`.

### Examples

#### CRC as a classic structure with static calculation methods

Here is an example which give hints on how explicitly use the structure, for a given string.

``` c++
#include <crc>
#include <iostream>

constexpr char const* str = "Hello, World!";
constexpr std::size_t length = std::strlen(str);

int main() {
  std::cout.hex();
  
  /* Passing c-string as a byte string. */
  std::cout << "crc64(\"" << str << "\", " << length << ") = " << crc64::calculate(reinterpret_cast<std::uint8_t const*>(str), std::strlen(str)) << '\n'; 
  
  /* Passing c-string. */
  std::cout << "crc64(\"" << str << "\") = " << crc64::calculate(str) << '\n';
  
  /* Passing any 'Iterable' type i.e. having begin() and end() returning iterators on std::uint8_t (or equivalent). */
  std::cout << "crc64(\"" << str << "\"s) = " << crc64::calculate(std::string(str)) << '\n';
  
  return 0;
}
```

```
crc64("Hello, World!", 13) = 0xa6f2204da53a9036
crc64("Hello, World!") = 0xa6f2204da53a9036
crc64("Hello, World!"s) = 0xa6f2204da53a9036
```

#### CRC as a functional structure with `operator()` overloads

``` c++
#include <crc>
#include <iostream>

constexpr char const* str = "Hello, World!";
constexpr std::size_t length = std::strlen(str);

int main() {
  std::cout.hex();
  
  std::cout << "crc64(\"" << str << "\", " << length << ") = " << crc64()(reinterpret_cast<std::uint8_t const*>(str), std::strlen(str)) << '\n'; 
  std::cout << "crc64(\"" << str << "\") = " << crc64()(str) << '\n';
  std::cout << "crc64(\"" << str << "\"s) = " << crc64()(std::string(str)) << '\n';
  
  return 0;
}
```

```
crc64("Hello, World!", 13) = 0xa6f2204da53a9036
crc64("Hello, World!") = 0xa6f2204da53a9036
crc64("Hello, World!"s) = 0xa6f2204da53a9036
```

#### Various CRC implementations

``` c++
#include <crc>
#include <iostream>

constexpr char const* str = "Hello, World!";
constexpr std::size_t length = std::strlen(str);

int main() {
  std::cout.hex();
  
  /* CRC-64 is an alisas for CRC-64/ECMA (hence you can explicitly use crc64::ecma structure). */
  std::cout << "crc64(\"" << str << "\") = " << crc64(str) << '\n';
  /* CRC-64/ISO implementation. */
  std::cout << "crc64::iso(\"" << str << "\") = " << crc64::iso::calculate(std::string(str)) << '\n';
  /* CRC-64/WE implementation. */
  std::cout << "crc64::we(\"" << str << "\") = " << crc64::iso::calculate(std::string(str)) << '\n';
  /* CRC-64/XZ implementation. */
  std::cout << "crc64::xz(\"" << str << "\") = " << crc64::xz::calculate(std::string(str)) << '\n';
  
  /* ... and so on for each CRC (CRC-8, CRC-16, CRC-32 and CRC-64). Each structure use the exact same signatures. */
  
  return 0;
}
```

```
crc64("Hello, World!") = 0xa6f2204da53a9036
crc64::iso("Hello, World!") = 0xd176c45c2627d9c2
crc64::we("Hello, World!") = 0xd381633ff5fb5137
crc64::xz("Hello, World!") = 0x280633f05c647b2c
```

#### User-defined implementations

``` c++
#include <crc>
#include <iostream>

constexpr char const* str = "Hello, World!";

struct user_defined_crc : public libcrc::crc<std::uint64_t, user_defined_crc> {
public:
  /* Optional. */ 
  static constexpr char const* name = "CRC-64/USER-DEFINED";
  static constexpr std::array<char const*, 0> aliases = {};

  /* Mandatory. */
  static constexpr std::uint64_t polynomial = 0x42f0'e1eb'a9ea'3693ull;
  static constexpr std::uint64_t reversed_polynomial = 0xc96c'5795'd787'0f42ull;

  static constexpr std::uint64_t start_value = 0xdead'beef'dead'beafull;
  static constexpr std::uint64_t final_xor_value = 0xbaaa'aaaa'aaaa'aaadull;

  static constexpr bool is_reversed = false;
};

int main() {
  std::cout.hex();
  
  std::cout << "user_defined_crc(\"" << str << "\") = " << user_defined_crc::calculate(str) << '\n';

  return 0;
}
```

```
user_defined_crc("Hello, World!") = 0xd05d000c80f835a2
```

### Implementations support list

| Implementation   | Aliases                                                                | Polynomial (or reversed polynomial)  | `start_value`           | `final_xor_value`       |
| ---------------- | ---------------------------------------------------------------------- | :----------------------------------: | :---------------------: | :---------------------: |
| `CRC-8`          | `CRC-8/SMBUS`                                                          | `0x07`                               | `0x00`                  | `0x00`                  |
| `CRC-16`         | `CRC-16/ARC`, `ARC`, `CRC-16/LHA`, `CRC-IBM`                           | reversed `0xa001`                    | `0x0000`                | `0x0000`                |
| `CRC-16/MODBUS`  |                                                                        | reversed `0xa001`                    | `0xffff`                | `0x0000`                |
| `CRC-32`         | `CRC-32/ISO-HDLC`, `CRC-32/ADCCP`, `CRC-32/V-42`, `CRC-32/XZ`, `PKZIP` | reversed `0xedb8'8320`               | `0xffff'ffff`           | `0xffff'ffff`           |
| `CRC-64`         | `CRC-64/ECMA-182`                                                      | `0x42f0'e1eb'a9ea'3693`              | `0x0000'0000'0000'0000` | `0x0000'0000'0000'0000` |
| `CRC-64/GO-ISO`  |                                                                        | reversed `0xd800'0000'0000'0000`     | `0xffff'ffff'ffff'ffff` | `0xffff'ffff'ffff'ffff` |
| `CRC-64/WE`      |                                                                        | reversed `0x42f0'e1eb'a9ea'3693`     | `0xffff'ffff'ffff'ffff` | `0xffff'ffff'ffff'ffff` |
| `CRC-64/XZ`      | `CRC-64/GO-ECMA`                                                       | reversed `0xc96c'5795'd787'0f42`     | `0xffff'ffff'ffff'ffff` | `0xffff'ffff'ffff'ffff` |

## Architecture and platform support 

| Architecture     | Job Status: build       | Job Status: check       |
| ---------------- | ----------------------- | ----------------------- |
| `i386/debian`    | ![i386/debian:build]    | ![i386/debian:check]    |
| `amd64/debian`   | ![amd64/debian:build]   | ![amd64/debian:check]   |
| `arm32/debian`   | ![arm32/debian:build]   | ![arm32/debian:check]   |
| `arm64/debian`   | unavailable yet.        | unavailable yet.        |
| `ppc64le/debian` | ![ppc64le/debian:build] | ![ppc64le/debian:check] |
| `amd64/macos`    | unavailable yet.        | unavailable yet.        |
| `arm64/macos`    | unavailable yet.        | unavailable yet.        |
| `amd64/windows`  | ![amd64/windows:build]  | ![amd64/windows:check]  |

## Benchmarks



## License

Distributed under the MIT [license](https://gitlab.com/lucas.david/libcrc/-/tree/master-c++11/). See `LICENSE` for more information.

[i386/debian:build]: https://gitlab.com/lucas.david/libcrc/-/jobs/artifacts/master-c++11/raw/public/pipeline.svg?job=i386/debian:build
[amd64/debian:build]: https://gitlab.com/lucas.david/libcrc/-/jobs/artifacts/master-c++11/raw/public/pipeline.svg?job=amd64/debian:build
[arm32/debian:build]: https://gitlab.com/lucas.david/libcrc/-/jobs/artifacts/master-c++11/raw/public/pipeline.svg?job=arm32/debian:build
[arm64/debian:build]: https://gitlab.com/lucas.david/libcrc/-/jobs/artifacts/master-c++11/raw/public/pipeline.svg?job=arm64/debian:build
[ppc64le/debian:build]: https://gitlab.com/lucas.david/libcrc/-/jobs/artifacts/master-c++11/raw/public/pipeline.svg?job=ppc64le/debian:build
[amd64/macos:build]: https://gitlab.com/lucas.david/libcrc/-/jobs/artifacts/master-c++11/raw/public/pipeline.svg?job=amd64/macos:build
[arm64/macos:build]: https://gitlab.com/lucas.david/libcrc/-/jobs/artifacts/master-c++11/raw/public/pipeline.svg?job=arm64/macos:build
[amd64/windows:build]: https://gitlab.com/lucas.david/libcrc/-/jobs/artifacts/master-c++11/raw/public/pipeline.svg?job=amd64/windows:build

[i386/debian:check]: https://gitlab.com/lucas.david/libcrc/-/jobs/artifacts/master-c++11/raw/public/pipeline.svg?job=i386/debian:check
[amd64/debian:check]: https://gitlab.com/lucas.david/libcrc/-/jobs/artifacts/master-c++11/raw/public/pipeline.svg?job=amd64/debian:check
[arm32/debian:check]: https://gitlab.com/lucas.david/libcrc/-/jobs/artifacts/master-c++11/raw/public/pipeline.svg?job=arm32/debian:check
[arm64/debian:check]: https://gitlab.com/lucas.david/libcrc/-/jobs/artifacts/master-c++11/raw/public/pipeline.svg?job=arm64/debian:check
[ppc64le/debian:check]: https://gitlab.com/lucas.david/libcrc/-/jobs/artifacts/master-c++11/raw/public/pipeline.svg?job=ppc64le/debian:check
[amd64/macos:check]: https://gitlab.com/lucas.david/libcrc/-/jobs/artifacts/master-c++11/raw/public/pipeline.svg?job=amd64/macos:check
[arm64/macos:check]: https://gitlab.com/lucas.david/libcrc/-/jobs/artifacts/master-c++11/raw/public/pipeline.svg?job=arm64/macos:check
[amd64/windows:check]: https://gitlab.com/lucas.david/libcrc/-/jobs/artifacts/master-c++11/raw/public/pipeline.svg?job=amd64/windows:check