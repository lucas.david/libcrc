#include <crc>

#include <cstdint>
#include <cstdio>
#include <cstring>
#include <memory>
#include <string>

#if !defined(__LP32__) and !defined(__ILP32__) and !defined(__SILP32__) \
  and !defined(__LLP64__) and !defined(__LP64__) and !defined(__ILP64__) and !defined(__SILP64__)

  #if defined(_MSC_BUILD)
    #if defined(_WIN32)
      #define __ILP32__
    #elif defined(_WIN64)
      #define __LLP64__
    #else /* let's assume it's Win16 API then. */
      #define __LP32__
    #endif
  #else
    /* if LP32 2/4/4 */
    #if __SIZEOF_SHORT__ == 2 and __SIZEOF_INT__ == 2 and __SIZEOF_LONG__ == 4 and __SIZEOF_POINTER__ == 4
      #define __LP32__
    /* if ILP32 4/4/8, thus Unix and Unix-like systems */
    #elif __SIZEOF_SHORT__ == 2 and __SIZEOF_INT__ == 4 and __SIZEOF_LONG__ == 4 and __SIZEOF_POINTER__ == 4
      #define __ILP32__
    /* if SILP32 4/4/4/8 */
    #elif __SIZEOF_SHORT__ == 4 and __SIZEOF_INT__ == 4 and __SIZEOF_LONG__ == 4 and __SIZEOF_POINTER__ == 4
      #define __SILP32__
    /* if LLP64 4/4/8 */
    #elif __SIZEOF_SHORT__ == 2 and __SIZEOF_INT__ == 4 and __SIZEOF_LONG__ == 4 and __SIZEOF_POINTER__ == 8
      #define __LLP64__
    /* if LP64 4/8/8, thus Unix and Unix-like systems */
    #elif __SIZEOF_SHORT__ == 2 and __SIZEOF_INT__ == 4 and __SIZEOF_LONG__ == 8 and __SIZEOF_POINTER__ == 8
      #define __LP64__
    /* if ILP64 8/8/8, thus some 64-bit Unix systems */
    #elif __SIZEOF_SHORT__ == 2 and __SIZEOF_INT__ == 8 and __SIZEOF_LONG__ == 8 and __SIZEOF_POINTER__ == 8
      #define __ILP64__
    /* if SILP64 8/8/8/8 */
    #elif __SIZEOF_SHORT__ == 8 and __SIZEOF_INT__ == 8 and __SIZEOF_LONG__ == 8 and __SIZEOF_POINTER__ == 8
      #define __SILP64__
    #else
      #error "Unknown data model."
    #endif
  #endif

#endif

static bool check_conformance();

int main() { 
  return check_conformance() ? 0 : 1;
}

namespace detail {

  template<typename T>
  static std::pair<bool, typename T::value_type> check_conformance(std::uint8_t const* bytes, std::size_t size, typename T::value_type answer) noexcept {
    auto const calculated = T()(bytes, size);
    return {calculated == answer, calculated};
  }

  template<typename T>
  static std::pair<bool, typename T::value_type> check_conformance(char const* str, typename T::value_type answer) noexcept {
    auto const calculated = T()(str);
    return {calculated == answer, calculated};
  }

  template<typename T>
  static std::pair<bool, typename T::value_type> check_conformance(std::string const& iterable, typename T::value_type answer) noexcept {
    auto const calculated = T()(iterable);
    return {calculated == answer, calculated};
  }

  template<typename... Args>
  static std::string format(char const* fmt, Args&& ... args) {
    auto const length = static_cast<std::size_t>(std::snprintf(nullptr, 0, fmt, args...));
    auto buffer = std::unique_ptr<char[]>(new char[length]);
    std::sprintf(buffer.get(), fmt, args...);
    return std::string().assign(buffer.release(), length);
  }

  template<typename T = std::uint32_t>
  static std::string hex(T value) {
    #if defined(__LP32__)                                                                     /* 2/4/4 */
      return format("%#08lx", value);
    #elif defined(__ILP32__) or defined(__LLP64__) or defined(__LP64__) or defined(__ILP64__) /* 4/[4-8]/[4-8] */
      return format("%#08x", value);
    #else
      #error "Data model unsupported, check https://en.cppreference.com/w/cpp/language/types#Data_models for more informations."
    #endif
  }

  template<>
  std::string hex<std::uint16_t>(std::uint16_t value) {
    #if defined(__LP32__)                                                                     /* 2/4/4 */
      return format("%#04x", value);
    #elif defined(__ILP32__) or defined(__LLP64__) or defined(__LP64__) or defined(__ILP64__) /* 4/[4-8]/[4-8] */
      return format("%#08x", value);
    #else
      #error "Data model unsupported, check https://en.cppreference.com/w/cpp/language/types#Data_models for more informations."
    #endif
  }

  template<>
  std::string hex<std::uint64_t>(std::uint64_t value) {
    #if defined(__LP32__) or defined(__ILP32__) or defined(__LLP64__) /* [2-4]/4/[4-8] */
      return format("%#016llx", value);
    #elif defined(__LP64__)                                           /* 4/8/8 */
      return format("%#016lx", value);
    #elif defined(__ILP64__)                                          /* 8/8/8 */
      return format("%#016x", value);
    #else
      #error "Data model unsupported, check https://en.cppreference.com/w/cpp/language/types#Data_models for more informations."
    #endif
  }

  namespace print {

    template<typename T>
    static void check_failure_message(char const* name, char const* function, T calculated, T answer) {
      std::printf("FAILED (%s::%s)\n", name, function);
      std::printf("  had %s expected %s\n", hex(calculated).c_str(), hex(answer).c_str());
    }

    template<typename T>
    static void check_success_message(T value) { 
      std::puts("SUCCESS");
      std::printf("  %s\n", hex(value).c_str());
    }

  }

}

template<typename T>
static bool check_conformance(char const* str, typename T::value_type answer) {
  std::printf("%s: ", T::name);

  auto result = detail::check_conformance<T>(reinterpret_cast<std::uint8_t const*>(str), std::strlen(str), answer);
  if (!result.first) {
    detail::print::check_failure_message(T::name, "calculate(std::uint8_t const*, std::size_t) noexcept", result.second, answer);
    return false;
  }
  result = detail::check_conformance<T>(str, answer);
  if (!result.first) {
    detail::print::check_failure_message(T::name, "calculate(char const*) noexcept", result.second, answer);
    return false;
  }
  result = detail::check_conformance<T>(std::string(str), answer);
  if (!result.first) {
    detail::print::check_failure_message(T::name, "calculate(Iterable const&) noexcept", result.second, answer);
    return false;
  }

  detail::print::check_success_message(result.second);
  return true;
}

static bool check_conformance() {
  using namespace libcrc;

  char const* string = "Hello, World!";
  bool passed = true;
  
  std::printf("Checking CRC calculation correctness for following character string: \"%s\"\n", string);
  /** CRC-8 **/
  passed &= check_conformance<crc8>("Hello, World!", 0x87u);

  /** CRC-16 **/
  passed &= check_conformance<crc16>("Hello, World!", 0xfa4du);
  passed &= check_conformance<crc16::modbus>("Hello, World!", 0x114eu);

  /** CRC-32 **/
  passed &= check_conformance<crc32>("Hello, World!", 0xec4ac3d0ul);

  /** CRC-64 **/
  passed &= check_conformance<crc64>("Hello, World!", 0xa6f2204da53a9036ull);
  passed &= check_conformance<crc64::iso>("Hello, World!", 0xd176c45c2627d9c2ull);
  passed &= check_conformance<crc64::we>("Hello, World!", 0xd381633ff5fb5137ull);
  passed &= check_conformance<crc64::xz>("Hello, World!", 0x280633f05c647b2cull);

  return passed;
}