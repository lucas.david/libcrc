/**
 * Copyright (c) 2021 Lucas David
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef LIBCRC_LOOKUP
#define LIBCRC_LOOKUP

#include <libcrc/detail/bit.h++>

#include <array>
#include <cstdint>

namespace libcrc {

  namespace detail {

    template<typename Function, std::size_t N>
    struct compose_n {
    public:
      template<typename T, T Polynomial>
      constexpr T operator()(T value) const { 
        return compose_n<Function, N - 1>()
          .template operator()<T, Polynomial>(Function().template operator()<T, Polynomial>(value));
      }
    };

    template<typename Function>
    struct compose_n<Function, 0> {   
    public:
      template<typename T, T Polynomial>
      constexpr T operator()(T value) const { return value; }
    };

    namespace cpp14 {

      template<typename T, T... Integers>
      struct integer_sequence {
      public:
        using value_type = T;

      public:
        static constexpr std::size_t size() { return sizeof...(Integers); }
      };

      template<std::size_t... Indices>
      using index_sequence = integer_sequence<std::size_t, Indices...>;

      template<typename T, std::size_t N, T... Integers>
      struct make_integer_sequence : make_integer_sequence<T, N - 1, N - 1, Integers...> {};

      template<typename T, T... Integers>
      struct make_integer_sequence<T, 0, Integers...> : integer_sequence<T, Integers...> {};

      template<std::size_t N>
      using make_index_sequence = make_integer_sequence<std::size_t, N>;
      
      template<typename... T>
      using index_sequence_for = make_index_sequence<sizeof...(T)>;

    }

    struct lookup_value {
    public:
      template<typename T, T Polynomial>
      constexpr T operator()(T value) const noexcept {
        return value & detail::most_significant_bit<T>::mask 
          ? static_cast<T>(static_cast<T>(value << 1) ^ Polynomial)
          : static_cast<T>(value << 1);
      }
    };

    struct reversed_lookup_value {
    public:
      template<typename T, T ReversedPolynomial>
      constexpr T operator()(T value) const noexcept {
        return value & detail::least_significant_bit<T>::mask
          ? static_cast<T>(static_cast<T>(value >> 1) ^ ReversedPolynomial)
          : static_cast<T>(value >> 1);
      }
    };

    struct lookup {
    private:
      template<typename T, T Polynomial, std::size_t... Indices>
      static constexpr std::array<T, sizeof...(Indices)> compute(detail::cpp14::index_sequence<Indices...>) noexcept { 
        return {
          detail::compose_n<detail::lookup_value, 8>()
            .template operator()<T, Polynomial>(static_cast<T>(static_cast<T>(Indices) << detail::most_significant_byte<T>::shift))...
        };
      }

    public:
      template<typename T, T Polynomial>
      static constexpr std::array<T, 256> value() noexcept { 
        return compute<T, Polynomial>(detail::cpp14::make_index_sequence<256>());
      }
    };

    struct reversed_lookup {
    private:
      template<typename T, T ReversedPolynomial, std::size_t... Indices>
      static constexpr std::array<T, sizeof...(Indices)> compute(detail::cpp14::index_sequence<Indices...>) noexcept { 
        return {
          detail::compose_n<detail::reversed_lookup_value, 8>()
            .template operator()<T, ReversedPolynomial>(static_cast<T>(Indices))...
        };
      }

    public:
      template<typename T, T ReversedPolynomial>
      static constexpr std::array<T, 256> value() noexcept { 
        return compute<T, ReversedPolynomial>(detail::cpp14::make_index_sequence<256>());
      }
    };

  }

  template<typename T>
  std::array<T, 256> lookup(T polynomial) noexcept {
    std::array<T, 256> lookup = {};
    for (std::size_t index = 0; index != 256; ++index) {
      lookup[index] = static_cast<T>(static_cast<T>(index) << detail::most_significant_byte<T>::shift);
      for (unsigned int i = 0; i != 8; ++i) {
        lookup[index] = lookup[index] & detail::most_significant_bit<T>::mask 
          ? static_cast<T>(static_cast<T>(lookup[index] << 1) ^ polynomial)
          : static_cast<T>(lookup[index] << 1);
      }
    }
    return lookup;
  }

  template<typename T>
  std::array<T, 256> reversed_lookup(T reversed_polynomial) noexcept {
    std::array<T, 256> reversed_lookup = {};
    for (std::size_t index = 0; index != 256; ++index) {
      reversed_lookup[index] = static_cast<T>(index);
      for (unsigned int i = 0; i != 8; ++i) {
        reversed_lookup[index] = reversed_lookup[index] & detail::least_significant_bit<T>::mask
          ? static_cast<T>(static_cast<T>(reversed_lookup[index] >> 1) ^ reversed_polynomial)
          : static_cast<T>(reversed_lookup[index] >> 1);
      }
    }
    return reversed_lookup;
  }

}

#endif