#ifndef LIBCRC_CRC
#define LIBCRC_CRC

#include <libcrc/lookup.h++>
#include <libcrc/detail/bit.h++>

#include <array>
#include <cstdint>

namespace libcrc {

  template<typename T, typename Traits>
  struct crc {
  public:
    using value_type = T;

  public:
    static T calculate(std::uint8_t const* bytes, std::size_t length) noexcept {
      T crc = Traits::start_value;
      if (bytes) {
        for (std::size_t index = 0; index != length; ++index) {
          crc = update(crc, bytes[index]);
        }
      }
      return static_cast<T>(crc ^ Traits::final_xor_value);
    }

    static T calculate(char const* str) noexcept {
      T crc = Traits::start_value;
      if (str) {
        for (std::size_t index = 0; str[index] != '\0'; ++index) {
          crc = update(crc, (uint8_t)str[index]);
        }
      }
      return static_cast<T>(crc ^ Traits::final_xor_value);
    }

    template<typename Iterable>
    static T calculate(Iterable const& iterable) noexcept {
      static_assert(std::is_integral<typename Iterable::value_type>::value and sizeof(typename Iterable::value_type) == 1u, 
        "Iterable should be iterating on a integral of 1 byte long such as std::uint8_t, std::int8_t or char.'");
      T crc = Traits::start_value;
      for (auto it = iterable.begin(); it != iterable.end(); ++it) {
        crc = update(crc, static_cast<std::uint8_t>(*it));
      }
      return static_cast<T>(crc ^ Traits::final_xor_value);
    }

    static constexpr T update(T crc, std::uint8_t value) noexcept {
      return !Traits::is_reversed 
        ? static_cast<T>(static_cast<T>(crc << 8) 
          ^ detail::lookup::value<T, Traits::polynomial>()
              [static_cast<typename std::array<T, 256>::size_type>(detail::most_significant_byte<T>()(crc) ^ value)])
        : static_cast<T>(static_cast<T>(crc >> 8) 
          ^ detail::reversed_lookup::value<T, Traits::reversed_polynomial>()
              [static_cast<typename std::array<T, 256>::size_type>(static_cast<std::uint8_t>(crc) ^ value)]);
    }
    
  public:
    constexpr T operator()(std::uint8_t const* bytes, std::size_t length) const noexcept { return calculate(bytes, length); }

    constexpr T operator()(char const* str) const noexcept { return calculate(str); }

    template<typename Iterable>
    constexpr T operator()(Iterable const& iterable) const noexcept { return calculate(iterable); }
  };

}

#endif