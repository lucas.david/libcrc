/**
 * Copyright (c) 2021 Lucas David
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef LIBCRC_CRC16
#define LIBCRC_CRC16

#include <libcrc/crc.h++>
#include <libcrc/lookup.h++>

#include <array>
#include <cstdint>

namespace libcrc {

  struct crc16 : public crc<std::uint16_t, crc16> {
  public:
    /** @note aliasing CRC-16/ARC struct as CRC-16 struct. */
    struct arc;

    /** @struct CRC-16/MODBUS struct. */
    struct modbus;

  public:
    static constexpr char const* name = "CRC-16";
    static constexpr std::array<char const*, 4> aliases = {"ARC", "CRC-16/ARC", "CRC-16/LHA", "CRC-IBM"};

    static constexpr std::uint16_t polynomial = 0x8005u;
    static constexpr std::uint16_t reversed_polynomial = 0xa001u;

    static constexpr std::uint16_t start_value = 0x0000u;
    static constexpr std::uint16_t final_xor_value = 0x0000u;

    static constexpr bool is_reversed = true;
  };

  /** @note aliasing CRC-16/ARC struct as CRC-16 struct. */
  struct crc16::arc : public crc16 {};

  /** @struct CRC-16/MODBUS struct. */
  struct crc16::modbus : public crc<std::uint16_t, crc16::modbus> {
  public:
    static constexpr char const* name = "CRC-16/MODBUS";
    static constexpr std::array<char const*, 0> aliases = {};

    static constexpr std::uint16_t polynomial = 0x8005u;
    static constexpr std::uint16_t reversed_polynomial = 0xa001u;

    static constexpr std::uint16_t start_value = 0xffffu;
    static constexpr std::uint16_t final_xor_value = 0x0000u;

    static constexpr bool is_reversed = true;
  };

}

#endif