/**
 * Copyright (c) 2021 Lucas David
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef LIBCRC_CRC8
#define LIBCRC_CRC8

#include <libcrc/crc.h++>
#include <libcrc/lookup.h++>

#include <array>
#include <cstdint>

namespace libcrc {

  struct crc8 : public crc<std::uint8_t, crc8> {
  public:
    /** @note aliasing CRC-8/SMBUS struct as CRC-8 struct. */
    struct smbus;

  public:
    static constexpr char const* name = "CRC-8";
    static constexpr std::array<char const*, 1> aliases = {"CRC-8/SMBUS"};

    static constexpr std::uint8_t polynomial = 0x07u;
    static constexpr std::uint8_t reversed_polynomial = 0xe0u;

    static constexpr std::uint8_t start_value = 0x00u;
    static constexpr std::uint8_t final_xor_value = 0x00u;

    static constexpr bool is_reversed = false;
  };

  /** @note aliasing CRC-8/SMBUS struct as CRC-8 struct. */
  struct crc8::smbus : public crc8 {};

}

#endif