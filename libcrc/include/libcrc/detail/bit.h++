/**
 * Copyright (c) 2021 Lucas David
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef LIBCRC_DETAIL_BIT
#define LIBCRC_DETAIL_BIT

#include <cstdint>
#include <limits>
#include <type_traits>

namespace libcrc {

  namespace detail {

    template<typename T>
    struct least_significant_bit {
    public:
      static_assert(std::is_integral<T>::value and std::is_unsigned<T>::value,
        "libcrc::detail::least_significant_bit<T> requires T std::is_integral<T>::value and std::is_unsigned<T>::value.");

    public:
      /** @brief shift displacement value to move the least significant bit into 'first' position. */
      static constexpr std::size_t shift = 0u;
      /** @brief mask for the least significant bit. */
      static constexpr T mask = 0x01u;

    public:
      /** @brief returns if the least significant bit is 1 (true) or 0 (false). */
      constexpr bool operator()(T value) const noexcept { return static_cast<bool>(value & mask); }
    };

    template<typename T>
    struct least_significant_byte {
    public:
      static_assert(std::is_integral<T>::value and std::is_unsigned<T>::value,
        "libcrc::detail::least_significant_byte<T> requires T std::is_integral<T>::value and std::is_unsigned<T>::value.");

    public:
      /** @brief shift displacement value to move the least significant byte into 'first' position. */
      static constexpr std::size_t shift = 0u;
      /** @brief mask for the least significant byte. */
      static constexpr T mask = 0xffu;

    public:
      /** @brief returns if the least significant byte. */
      constexpr std::uint8_t operator()(T value) const noexcept { return static_cast<std::uint8_t>(value); }
    };

    template<typename T>
    struct most_significant_bit {
    public:
      static_assert(std::is_integral<T>::value and std::is_unsigned<T>::value,
        "libcrc::detail::most_significant_bit<T> requires T std::is_integral<T>::value and std::is_unsigned<T>::value.");

    public:
      /** @brief shift displacement value to move the most significant bit into 'first' position. */
      static constexpr std::size_t shift = sizeof(T) * 8u - 1u;
      /** @brief mask for the most significant bit. */
      static constexpr T mask = static_cast<T>(std::numeric_limits<T>::max() << shift);

    public:
      /** @brief returns if the most significant bit is 1 (true) or 0 (false). */
      constexpr bool operator()(T value) const noexcept { return static_cast<bool>(value & mask); }
    };

    template<typename T>
    struct most_significant_byte {
    public:
      static_assert(std::is_integral<T>::value and std::is_unsigned<T>::value, 
        "libcrc::detail::most_significant_byte<T> requires T std::is_integral<T>::value and std::is_unsigned<T>::value.");

    public:
      /** @brief shift displacement value to move the most significant byte into 'first' position. */
      static constexpr std::size_t shift = (sizeof(T) - 1u) * 8u;
      /** @brief mask for the most significant byte. */
      static constexpr T mask = static_cast<T>(std::numeric_limits<T>::max() << shift);

    public:
      /** @brief returns if the most significant byte. */
      constexpr std::uint8_t operator()(T value) const noexcept { return static_cast<std::uint8_t>(value >> shift); }
    };

  }
  
}

#endif