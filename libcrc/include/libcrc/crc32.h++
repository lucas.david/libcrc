/**
 * Copyright (c) 2021 Lucas David
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef LIBCRC_CRC32
#define LIBCRC_CRC32

#include <libcrc/crc.h++>
#include <libcrc/lookup.h++>

#include <cstdint>

namespace libcrc {

  struct crc32 : public crc<std::uint32_t, crc32> {
  public:
    /** @note aliasing CRC-32/ISO-HDLC struct as CRC-32 struct. */
    struct iso;

  public:
    static constexpr char const* name = "CRC-32";
    static constexpr std::array<char const*, 5> aliases = {"CRC-32/ISO-HDLC", "CRC-32/ADCCP", "CRC-32/V-42", "CRC-32/XZ", "PKZIP"};

    static constexpr std::uint32_t polynomial = 0x04c11db7ul;
    static constexpr std::uint32_t reversed_polynomial = 0xedb88320ul;

    static constexpr std::uint32_t start_value = 0xfffffffful;
    static constexpr std::uint32_t final_xor_value = 0xfffffffful;

    static constexpr bool is_reversed = true;
  };

  /** @note aliasing CRC-32/ISO-HDLC struct as CRC-32 struct. */
  struct crc32::iso : public crc32 {};

}

#endif