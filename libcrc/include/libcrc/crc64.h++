/**
 * Copyright (c) 2021 Lucas David
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef LIBCRC_CRC64
#define LIBCRC_CRC64

#include <libcrc/crc.h++>
#include <libcrc/lookup.h++>

#include <cstdint>

namespace libcrc {

  /** @struct crc64 {@alias crc64::ecma}
   *
   * CRC-64 {@alias CRC-64/ECMA-182} structure definition.
   * CRC-64/ECMA-182 is defined within ECMA-182 (p.51) for data interchange on magnetic tape cartridges.
   */
  struct crc64 : public crc<std::uint64_t, crc64> {
  public:
    /** @note aliasing CRC-64/ECMA-284 struct as CRC-64 struct. */
    struct ecma;

    /** @struct crc64::iso
     *
     * CRC-64/GO-ISO is usually used in HDLC (High-Level Data Link Control; alias ISO 3309), and protein databases.
     *
     * @note CRC-64/GO-ISO is considered weak for hashing, check_conformance CRC-64/ECMA-182 for more reliable alternative.
     */
    struct iso;

    /** @struct crc64::we */
    struct we;

    /** @struct crc64::xz
     *
     * CRC-64/XZ or {@alias CRC-64/GO-ECMA} is used within XZ utils' data compression software.
     *
     * @note CRC-64/XZ is commonly misidentified as ECMA. {@see crc64::ecma} {@link CRC-64/ECMA-182}{https://reveng.sourceforge.io/crc-catalogue/17plus.htm#crc.cat.crc-64-ecma-182}
     *  Nonetheless, CRC-64/XZ is based on reversed CRC-64/ECMA-182's polynomial
     *  but uses different starting and final xor values.
     */
    struct xz;

  public:
    static constexpr char const* name = "CRC-64";
    static constexpr std::array<char const*, 1> aliases = {"CRC-64/ECMA-182"};

    static constexpr std::uint64_t polynomial = 0x42f0e1eba9ea3693ull;
    static constexpr std::uint64_t reversed_polynomial = 0xc96c5795d7870f42ull;

    static constexpr std::uint64_t start_value = 0x0000000000000000ull;
    static constexpr std::uint64_t final_xor_value = 0x0000000000000000ull;

    static constexpr bool is_reversed = false;
  };

  /** @note aliasing CRC-64/ECMA-284 struct as CRC-64 struct. */
  struct crc64::ecma : public crc64 {};

  struct crc64::iso : public crc<std::uint64_t, crc64::iso> {
  public:
    static constexpr char const* name = "CRC-64/GO-ISO";
    static constexpr std::array<char const*, 0> aliases = {};

    static constexpr std::uint64_t polynomial = 0x000000000000001bull;
    static constexpr std::uint64_t reversed_polynomial = 0xd800000000000000ull;

    static constexpr std::uint64_t start_value = 0xffffffffffffffffull;
    static constexpr std::uint64_t final_xor_value = 0xffffffffffffffffull;

    static constexpr bool is_reversed = true;
  };

  struct crc64::we : public crc<std::uint64_t, crc64::we> {
  public:
    static constexpr char const* name = "CRC-64/WE";
    static constexpr std::array<char const*, 0> aliases = {};

    static constexpr std::uint64_t polynomial = 0x42f0e1eba9ea3693ull;          /* <== same as crc64::ecma */
    static constexpr std::uint64_t reversed_polynomial = 0xc96c5795d7870f42ull; /* <== same as crc64::ecma */

    static constexpr std::uint64_t start_value = 0xffffffffffffffffull;
    static constexpr std::uint64_t final_xor_value = 0xffffffffffffffffull;

    static constexpr bool is_reversed = false;
  };

  struct crc64::xz : public crc<std::uint64_t, crc64::xz> {
  public:
    static constexpr char const* name = "CRC-64/XZ";
    static constexpr std::array<char const*, 1> aliases = {"CRC-64/GO-ECMA"};

    static constexpr std::uint64_t polynomial = 0x42f0e1eba9ea3693ull;          /* <== same as crc64::ecma */
    static constexpr std::uint64_t reversed_polynomial = 0xc96c5795d7870f42ull; /* <== same as crc64::ecma */

    static constexpr std::uint64_t start_value = 0xffffffffffffffffull;
    static constexpr std::uint64_t final_xor_value = 0xffffffffffffffffull;

    static constexpr bool is_reversed = true;
  };

}

#endif